resource "google_storage_bucket" "terraform-state" {
  count = var.enable_buckets.terraform-state ? 1 : 0

  name     = "gitlab-${var.environment}-terraform-state"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = var.terraform_state_versioning
  }

  storage_class = var.storage_class

  labels = merge(lookup(var.labels, "infrastructure", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-terraform-state"
  })

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age        = var.terraform_state_age
      with_state = "ARCHIVED"
    }
  }

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }
}

resource "google_storage_bucket_iam_member" "terraform-state-admin" {
  for_each = var.enable_buckets.terraform-state ? var.gcs_service_account_emails : []

  bucket = google_storage_bucket.terraform-state[0].name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${each.value}"
}
