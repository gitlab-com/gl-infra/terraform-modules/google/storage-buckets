resource "google_storage_bucket" "artifacts" {
  count = var.enable_buckets.artifacts ? 1 : 0

  name     = "gitlab-${var.environment}-artifacts"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = var.artifacts_versioning
  }

  storage_class = var.storage_class

  autoclass {
    enabled                = var.artifacts_autoclass_enabled
    terminal_storage_class = var.artifacts_autoclass_terminal_class
  }

  labels = merge(lookup(var.labels, "build_artifacts", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-artifacts"
  })

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age        = var.artifact_age
      with_state = "ARCHIVED"
    }
  }

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }
}

resource "google_storage_bucket_iam_member" "artifacts-admin" {
  for_each = var.enable_buckets.artifacts ? var.gcs_service_account_emails : []

  bucket = google_storage_bucket.artifacts[0].name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${each.value}"
}
