resource "google_storage_bucket" "log" {
  count = var.enable_buckets.logging-archive ? 1 : 0

  name     = "gitlab-${var.environment}-logging-archive"
  location = local.storage_location

  uniform_bucket_level_access = true

  labels = merge(lookup(var.labels, "shared", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-logging-archive"
  })

  storage_class = var.archived_logs_storage_class

  lifecycle_rule {
    action {
      type          = "SetStorageClass"
      storage_class = "ARCHIVE"
    }

    condition {
      age        = var.archived_logs_archive_move_age
      with_state = "ANY"
    }
  }

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age        = var.archived_logs_deletion_age
      with_state = "ANY"
    }
  }

  # versioning is incompatible with retention_policy (see below)
  versioning {
    enabled = false
  }

  # lock objects for 1 year in order to prevent tampering
  #
  # https://gitlab.com/gitlab-com/gl-security/compliance/observation-management/-/issues/31
  retention_policy {
    is_locked        = true
    retention_period = 31536000 # 365 days
  }
}

resource "google_storage_bucket_iam_member" "archived-logs-viewer" {
  for_each = var.enable_buckets.logging-archive ? setunion(
    ["serviceAccount:${var.service_account_email}"],
    var.archived_logs_creator_access,
    var.archived_logs_viewer_access,
  ) : []

  bucket = google_storage_bucket.log[0].name
  role   = "roles/storage.objectViewer"
  member = each.value
}

resource "google_storage_bucket_iam_member" "archived-logs-legacy-viewer-reader" {
  for_each = var.enable_buckets.logging-archive ? toset(var.archived_logs_creator_access) : []

  bucket = google_storage_bucket.log[0].name
  role   = "roles/storage.legacyBucketReader"
  member = each.value
}

resource "google_storage_bucket_iam_member" "archived-logs-creator-creator" {
  for_each = var.enable_buckets.logging-archive ? toset(var.archived_logs_creator_access) : []

  bucket = google_storage_bucket.log[0].name
  role   = "roles/storage.objectCreator"
  member = each.value
}
