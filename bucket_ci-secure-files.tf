resource "google_storage_bucket" "ci-secure-files" {
  count = var.enable_buckets.ci-secure-files ? 1 : 0

  name     = "gitlab-${var.environment}-ci-secure-files"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = var.ci_secure_files_versioning
  }

  storage_class = var.storage_class

  labels = merge(lookup(var.labels, "continuous_integration", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-ci-secure-files"
  })

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age        = var.ci_secure_files_age
      with_state = "ARCHIVED"
    }
  }

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }
}

resource "google_storage_bucket_iam_member" "ci-secure-files-admin" {
  for_each = var.enable_buckets.ci-secure-files ? var.gcs_service_account_emails : []

  bucket = google_storage_bucket.ci-secure-files[0].name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${each.value}"
}
