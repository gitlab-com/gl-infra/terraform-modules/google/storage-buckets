resource "google_storage_bucket" "chef-bootstrap" {
  count = var.enable_buckets.chef-bootstrap ? 1 : 0

  name     = "gitlab-${var.environment}-chef-bootstrap"
  location = local.storage_location

  uniform_bucket_level_access = true

  labels = merge(lookup(var.labels, "infrastructure", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-chef-bootstrap"
  })

  versioning {
    enabled = var.chef_bootstrap_versioning
  }

  storage_class = var.storage_class
}

resource "google_storage_bucket_iam_member" "chef-bootstrap-viewer" {
  count = var.enable_buckets.chef-bootstrap ? 1 : 0

  bucket = google_storage_bucket.chef-bootstrap[0].name
  role   = "roles/storage.objectViewer"
  member = "serviceAccount:${var.service_account_email}"
}
