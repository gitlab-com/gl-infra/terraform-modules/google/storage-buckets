resource "google_storage_bucket" "security" {
  count = var.enable_buckets.security ? 1 : 0

  name     = "gitlab-${var.environment}-security"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = var.security_versioning
  }

  storage_class = var.storage_class

  labels = merge(lookup(var.labels, "security", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-security"
  })

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }
}

resource "google_storage_bucket_iam_member" "security-admin" {
  count = var.enable_buckets.security ? 1 : 0

  bucket = google_storage_bucket.security[0].name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${var.service_account_email}"
}
