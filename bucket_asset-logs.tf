resource "google_storage_bucket" "assets-logs" {
  count = var.enable_buckets.assets-logs ? 1 : 0

  name     = "gitlab-${var.environment}-assets-logs"
  location = local.storage_location

  storage_class = var.storage_class

  uniform_bucket_level_access = true

  versioning {
    enabled = var.assets_logs_versioning
  }

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age        = var.assets_log_age
      with_state = "LIVE"
    }
  }

  labels = merge(lookup(var.labels, "shared", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-assets-logs"
  })
}
