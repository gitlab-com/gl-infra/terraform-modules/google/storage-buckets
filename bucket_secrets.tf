resource "google_storage_bucket_iam_member" "secrets-viewer" {
  for_each = var.enable_buckets.secrets ? setunion(
    ["serviceAccount:${var.service_account_email}"],
    var.secrets_viewer_access
  ) : []

  bucket = "gitlab-${var.environment}-secrets"
  role   = "roles/storage.objectViewer"
  member = each.value
}

resource "google_storage_bucket_iam_member" "secrets-admin" {
  for_each = var.enable_buckets.secrets ? toset(var.secrets_admin_access) : []

  bucket = "gitlab-${var.environment}-secrets"
  role   = "roles/storage.objectAdmin"
  member = each.value
}
