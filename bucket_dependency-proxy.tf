resource "google_storage_bucket" "dependency-proxy" {
  count = var.enable_buckets.dependency-proxy ? 1 : 0

  name     = "gitlab-${var.environment}-dependency-proxy"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = var.dependency_proxy_versioning
  }

  storage_class = var.dependency_proxy_storage_class

  labels = merge(lookup(var.labels, "dependency_proxy", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-dependency-proxy"
  })

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age        = var.dependency_proxy_age
      with_state = "ARCHIVED"
    }
  }

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }
}

resource "google_storage_bucket_iam_member" "dependency-proxy-admin" {
  for_each = var.enable_buckets.dependency-proxy ? var.gcs_service_account_emails : []

  bucket = google_storage_bucket.dependency-proxy[0].name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${each.value}"
}
