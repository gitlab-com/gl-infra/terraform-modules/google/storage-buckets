output "logging_bucket_name" {
  value = one(google_storage_bucket.log[*].name)
}

output "chef_bootstrap_bucket_name" {
  value = one(google_storage_bucket.chef-bootstrap[*].name)
}

output "prometheus_service_account_email" {
  value = var.enable_buckets.prometheus ? google_service_account.prometheus-sa[0].email : null
}

output "prometheus_service_account_name" {
  value = var.enable_buckets.prometheus ? google_service_account.prometheus-sa[0].name : null
}
