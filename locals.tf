locals {
  # Default storage location, required
  # by google_storage_bucket resorces
  storage_location = "US"

  prometheus_sa_id = coalesce(var.prometheus_sa_id, "${var.environment}-prometheus-sa")

}
