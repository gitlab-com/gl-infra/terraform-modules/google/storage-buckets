resource "google_storage_bucket" "gitlab-pages" {
  count = var.enable_buckets.pages ? 1 : 0

  name     = "gitlab-${var.environment}-pages"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = var.pages_objects_versioning
  }

  storage_class = var.storage_class

  labels = merge(lookup(var.labels, "pages", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-pages"
  })

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }
}

resource "google_storage_bucket_iam_member" "gitlab-pages-admin" {
  for_each = var.enable_buckets.pages ? var.gcs_service_account_emails : []

  bucket = google_storage_bucket.gitlab-pages[0].name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${each.value}"
}
