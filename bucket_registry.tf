resource "google_storage_bucket" "registry" {
  count = var.enable_buckets.registry ? 1 : 0

  name     = "gitlab-${var.environment}-registry"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = var.registry_versioning
  }

  storage_class = var.storage_class

  labels = merge(lookup(var.labels, "registry", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-registry"
  })

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      days_since_noncurrent_time = var.registry_days_since_noncurrent_time
    }
  }

  autoclass {
    enabled                = var.registry_autoclass_enabled
    terminal_storage_class = var.registry_autoclass_terminal_class
  }

  # Source https://github.com/terraform-google-modules/terraform-google-cloud-storage/blob/master/main.tf
  dynamic "lifecycle_rule" {
    for_each = var.registry_lifecycle_rules
    content {
      action {
        type          = lifecycle_rule.value.action.type
        storage_class = lookup(lifecycle_rule.value.action, "storage_class", null)
      }
      condition {
        age                        = lookup(lifecycle_rule.value.condition, "age", null)
        created_before             = lookup(lifecycle_rule.value.condition, "created_before", null)
        with_state                 = lookup(lifecycle_rule.value.condition, "with_state", contains(keys(lifecycle_rule.value.condition), "is_live") ? (lifecycle_rule.value.condition["is_live"] ? "LIVE" : null) : null)
        matches_storage_class      = contains(keys(lifecycle_rule.value.condition), "matches_storage_class") ? split(",", lifecycle_rule.value.condition["matches_storage_class"]) : null
        matches_prefix             = contains(keys(lifecycle_rule.value.condition), "matches_prefix") ? split(",", lifecycle_rule.value.condition["matches_prefix"]) : null
        matches_suffix             = contains(keys(lifecycle_rule.value.condition), "matches_suffix") ? split(",", lifecycle_rule.value.condition["matches_suffix"]) : null
        num_newer_versions         = lookup(lifecycle_rule.value.condition, "num_newer_versions", null)
        custom_time_before         = lookup(lifecycle_rule.value.condition, "custom_time_before", null)
        days_since_custom_time     = lookup(lifecycle_rule.value.condition, "days_since_custom_time", null)
        days_since_noncurrent_time = lookup(lifecycle_rule.value.condition, "days_since_noncurrent_time", null)
        noncurrent_time_before     = lookup(lifecycle_rule.value.condition, "noncurrent_time_before", null)
      }
    }
  }
}

resource "google_storage_bucket_iam_member" "registry-admin" {
  for_each = var.enable_buckets.registry ? var.gcs_service_account_emails : []

  bucket = google_storage_bucket.registry[0].name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${each.value}"
}

resource "google_storage_bucket_iam_member" "registry-cdn-viewer" {
  count = var.enable_buckets.registry && var.enable_registry_cdn ? 1 : 0

  bucket = google_storage_bucket.registry[0].name
  role   = "roles/storage.objectViewer"
  member = "serviceAccount:service-${data.google_project.project.number}@cloud-cdn-fill.iam.gserviceaccount.com"
}
