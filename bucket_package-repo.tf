resource "google_storage_bucket" "package-repo" {
  count = var.enable_buckets.package-repo ? 1 : 0

  name     = "gitlab-${var.environment}-package-repo"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = var.package_repo_versioning
  }

  storage_class = var.storage_class

  labels = merge(lookup(var.labels, "registry", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-package-repo"
  })

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age        = var.package_repo_age
      with_state = "ARCHIVED"
    }
  }

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }
}

resource "google_storage_bucket_iam_member" "package-repo-admin" {
  for_each = var.enable_buckets.package-repo ? var.gcs_service_account_emails : []

  bucket = google_storage_bucket.package-repo[0].name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${each.value}"
}
