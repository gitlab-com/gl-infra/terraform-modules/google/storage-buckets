##################################
#
# Logging for StackDriver
#
##################################

resource "google_logging_project_sink" "log" {
  count = var.enable_buckets.logging-archive-sink ? 1 : 0

  name = "${var.environment}-logging-sink"

  destination = "storage.googleapis.com/${google_storage_bucket.log[0].name}"
  filter      = var.archive_sink_filter

  # Use a unique writer (creates a unique service account used for writing)
  unique_writer_identity = true
}

resource "google_project_iam_member" "project-log-creator" {
  count = var.enable_buckets.logging-archive-sink ? 1 : 0

  project = var.project
  role    = "roles/storage.objectCreator"
  member  = google_logging_project_sink.log[0].writer_identity
}
