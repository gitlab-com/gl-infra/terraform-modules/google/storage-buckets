resource "google_storage_bucket" "external-diffs" {
  count = var.enable_buckets.external-diffs ? 1 : 0

  name     = "gitlab-${var.environment}-external-diffs"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = var.external_diffs_versioning
  }

  storage_class = var.storage_class

  labels = merge(lookup(var.labels, "code_review", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-external-diffs"
  })

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age        = var.external_diffs_age
      with_state = "ARCHIVED"
    }
  }

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }
}

resource "google_storage_bucket_iam_member" "external-diffs-admin" {
  for_each = var.enable_buckets.external-diffs ? var.gcs_service_account_emails : []

  bucket = google_storage_bucket.external-diffs[0].name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${each.value}"
}
