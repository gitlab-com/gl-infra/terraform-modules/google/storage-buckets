# Temporary bucket to store flight api Output Blobs
resource "google_storage_bucket" "flightapi" {
  count         = var.enable_buckets.flightapi ? 1 : 0
  name          = "gitlab-${var.environment}-flightapi"
  location      = "US"
  force_destroy = true

  uniform_bucket_level_access = true

  storage_class = var.storage_class

  labels = merge(lookup(var.labels, "infrastructure", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-flightapi"
  })

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }
}

resource "google_storage_bucket_iam_member" "flightapi_gitlab_kas" {
  for_each = var.enable_buckets.flightapi ? var.kas_service_account_emails : []
  bucket   = google_storage_bucket.flightapi[0].name
  role     = "roles/storage.objectAdmin"
  member   = "serviceAccount:${each.value}"
}