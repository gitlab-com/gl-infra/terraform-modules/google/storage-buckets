resource "google_storage_bucket" "periodic-host-profile" {
  count = var.enable_buckets.periodic-host-profile ? 1 : 0

  name     = "gitlab-${var.environment}-periodic-host-profile"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = false
  }

  storage_class = var.storage_class

  labels = merge(lookup(var.labels, "infrastructure", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-periodic-host-profile"
  })

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age        = var.periodic_host_profile_log_age
      with_state = "ANY"
    }
  }
}

resource "google_storage_bucket_iam_member" "periodic-host-profile-admin-member-service-account" {
  count = var.enable_buckets.periodic-host-profile ? 1 : 0

  bucket = google_storage_bucket.periodic-host-profile[0].name
  role   = "roles/storage.objectAdmin"
  member = google_service_account.periodic-host-profile[0].member
}

resource "google_storage_bucket_iam_member" "periodic-host-profile-viewer-member-sg" {
  count = var.enable_buckets.periodic-host-profile ? 1 : 0

  bucket = google_storage_bucket.periodic-host-profile[0].name
  role   = "roles/storage.objectViewer"
  member = "group:${var.gcp_security_group_host_profiles_email}"
}
