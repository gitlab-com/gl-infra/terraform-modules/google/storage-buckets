resource "google_storage_bucket" "storage-logs" {
  count = var.enable_buckets.storage-logs ? 1 : 0

  name     = "gitlab-${var.environment}-storage-logs"
  location = local.storage_location

  storage_class = var.storage_class

  uniform_bucket_level_access = true

  versioning {
    enabled = var.storage_logs_versioning
  }

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age        = var.storage_log_age
      with_state = "LIVE"
    }
  }

  labels = merge(lookup(var.labels, "infrastructure", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-storage-logs"
  })
}

resource "google_storage_bucket_iam_member" "storage-analytics" {
  count = var.enable_buckets.storage-logs ? 1 : 0

  bucket = google_storage_bucket.storage-logs[0].name
  role   = "roles/storage.legacyBucketWriter"
  member = "group:${var.gcs_storage_analytics_group_email}"
}
