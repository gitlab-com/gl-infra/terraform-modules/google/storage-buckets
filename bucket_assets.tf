resource "google_storage_bucket" "assets" {
  count = var.enable_buckets.assets ? 1 : 0

  name     = "gitlab-${var.environment}-assets"
  location = local.storage_location

  // ACLs are currently required to make asset objects public
  // https://gitlab.com/gitlab-com/gl-infra/deploy-tooling/-/blob/b3070eb85e261d8552b60f311f8c52823f5cb17f/bin/fetch-and-upload-assets#L155
  uniform_bucket_level_access = false

  versioning {
    enabled = var.assets_versioning
  }

  storage_class = var.storage_class

  labels = merge(lookup(var.labels, "shared", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-assets"
  })

  logging {
    log_bucket = google_storage_bucket.assets-logs[0].name
  }

  website {
    main_page_suffix = var.assets_main_page_suffix
    not_found_page   = var.assets_not_found_page
  }

  dynamic "cors" {
    for_each = var.assets_cors
    content {
      origin          = cors.value.origin
      method          = cors.value.method
      response_header = cors.value.response_header
      max_age_seconds = cors.value.max_age_seconds
    }
  }
}

resource "google_storage_bucket_iam_member" "assets-admin" {
  for_each = var.enable_buckets.assets ? toset([
    "serviceAccount:${var.service_account_email}",
    # Necessary for write access from the ops account
    "serviceAccount:${var.service_account_email_ops}",
  ]) : []

  bucket = google_storage_bucket.assets[0].name
  role   = "roles/storage.objectAdmin"
  member = each.value
}
