resource "google_storage_bucket" "ci-cache" {
  count = var.enable_buckets.ci-cache ? 1 : 0

  name     = "gitlab-${var.environment}-ci-cache"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = var.artifacts_versioning
  }

  storage_class = var.storage_class

  labels = merge(lookup(var.labels, "shared", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-ci-cache"
  })

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }
}

resource "google_storage_bucket_iam_member" "ci-cache" {
  for_each = var.enable_buckets.ci-cache ? var.gcs_service_account_emails : []

  bucket = google_storage_bucket.ci-cache[0].name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${each.value}"
}