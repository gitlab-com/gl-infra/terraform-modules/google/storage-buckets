# Storage buckets that are common across all environments
variable "environment" {
  type = string
}

variable "service_account_email" {
  type = string
}

variable "project" {
  type = string
}

variable "enable_registry_cdn" {
  type    = bool
  default = false
}

variable "enable_buckets" {
  type = object({
    artifacts                     = optional(bool, true)
    assets                        = optional(bool, true)
    assets-logs                   = optional(bool, true)
    backups                       = optional(bool, false)
    chef-bootstrap                = optional(bool, true)
    ci-cache                      = optional(bool, false)
    ci-secure-files               = optional(bool, true)
    deleted-project-group-exports = optional(bool, false)
    dependency-proxy              = optional(bool, true)
    external-diffs                = optional(bool, true)
    flightapi                     = optional(bool, false)
    lfs-objects                   = optional(bool, true)
    logging-archive               = optional(bool, true)
    logging-archive-sink          = optional(bool, false)
    package-repo                  = optional(bool, true)
    pages                         = optional(bool, true)
    periodic-host-profile         = optional(bool, true)
    prometheus                    = optional(bool, true)
    redis-analysis                = optional(bool, true)
    registry                      = optional(bool, true)
    secrets                       = optional(bool, true)
    security                      = optional(bool, true)
    storage-logs                  = optional(bool, true)
    terraform-state               = optional(bool, true)
    thanos-rules                  = optional(bool, false)
    uploads                       = optional(bool, true)
  })
  description = "Toggle bucket creation."
  default     = {}
}

variable "secrets_viewer_access" {
  type        = list(string)
  description = "Accounts which require viewer access to our environment secrets"
  default     = []
}

variable "secrets_admin_access" {
  type        = list(string)
  description = "Accounts which require admin access to our environment secrets"
  default     = []
}

variable "archived_logs_viewer_access" {
  type        = list(string)
  description = "Accounts which require viewer access to archived logs"
  default     = []
}

variable "archived_logs_creator_access" {
  type        = list(string)
  description = "Accounts which require object creator access to archived logs"
  default     = []
}

variable "gcs_service_account_emails" {
  type        = set(string)
  description = "Email address of the service accounts for the GitLab deployment to grant access to its GCS buckets"
}

variable "kas_service_account_emails" {
  description = "The email of the service accounts for GitLab KAS"
  type        = set(string)
  default     = []
}

# https://cloud.google.com/storage/docs/access-logs
variable "gcs_storage_analytics_group_email" {
  type    = string
  default = "cloud-storage-analytics@google.com"
}

# This is used for write access to the assets bucket
# from the ops account. It is necessary so we can upload
# assets and serve them directly from object storage.

variable "service_account_email_ops" {
  type    = string
  default = "asset-uploader@gitlab-ops.iam.gserviceaccount.com"
}

variable "gcp_security_group_host_profiles_email" {
  type        = string
  default     = "gcp-host-profiles-sg@gitlab.com"
  description = "A Google Group containing team members who need viewer permissions for the 'periodic-host-profile' bucket"
}

### Versioning on #################

variable "chef_bootstrap_versioning" {
  type    = bool
  default = true
}

variable "uploads_versioning" {
  type    = bool
  default = true
}

variable "external_diffs_versioning" {
  type    = bool
  default = true
}

variable "lfs_objects_versioning" {
  type    = bool
  default = true
}

variable "ci_secure_files_versioning" {
  type    = bool
  default = true
}

variable "artifacts_versioning" {
  type    = bool
  default = true
}

variable "artifacts_autoclass_enabled" {
  description = "Enables autoclass on the artifacts bucket"
  default     = false
  type        = bool
}

variable "artifacts_autoclass_terminal_class" {
  description = "Sets the lowest storage class that Autoclass will move objects to"
  default     = "ARCHIVE"
  type        = string
}

variable "package_repo_versioning" {
  type    = bool
  default = true
}

variable "prometheus_versioning" {
  type    = bool
  default = true
}

variable "registry_versioning" {
  type    = bool
  default = true
}

variable "security_versioning" {
  type    = bool
  default = true
}

variable "assets_versioning" {
  type    = bool
  default = true
}

variable "terraform_state_versioning" {
  type    = bool
  default = true
}

## Versioning off

variable "storage_logs_versioning" {
  type    = bool
  default = false
}

variable "assets_logs_versioning" {
  type    = bool
  default = false
}

variable "dependency_proxy_versioning" {
  type    = bool
  default = false
}

# Generally no need for versioning here; files will be date-versioned
variable "redis_analysis_versioning" {
  type    = bool
  default = false
}

# Generally no need for versioning here; rules are git versioned
variable "thanos_rules_versioning" {
  type    = bool
  default = false
}

# GitLab Pages by their nature are versioned in git
variable "pages_objects_versioning" {
  type    = bool
  default = false
}

#############################

variable "artifact_age" {
  type    = number
  default = 7
}

variable "upload_age" {
  type    = number
  default = 7
}

variable "external_diffs_age" {
  type    = number
  default = 30
}

variable "lfs_object_age" {
  type    = number
  default = 30
}

variable "ci_secure_files_age" {
  type    = number
  default = 30
}

variable "package_repo_age" {
  type    = number
  default = 30
}

variable "terraform_state_age" {
  type    = number
  default = 30
}

variable "storage_class" {
  type    = string
  default = "STANDARD"
}

variable "dependency_proxy_age" {
  type    = number
  default = 30
}

variable "dependency_proxy_storage_class" {
  type    = string
  default = "STANDARD"
}

variable "archived_logs_storage_class" {
  type    = string
  default = "MULTI_REGIONAL"
}

variable "thanos_rules_storage_class" {
  type    = string
  default = "STANDARD"
}

variable "archived_logs_archive_move_age" {
  type        = number
  description = "Objects older than this many days will be moved to archive storage"
  default     = 15
}

variable "archived_logs_deletion_age" {
  type        = number
  description = "Objects older than this many days will be deleted"
  default     = 365
}

variable "storage_log_age" {
  type    = number
  default = 7
}

variable "redis_analysis_log_age" {
  type        = number
  description = "Redis analysis logs older than this many days will be deleted"
  default     = 1825 # 5 years
}

variable "periodic_host_profile_log_age" {
  type        = number
  default     = 30
  description = "Delete profiles older than this many days"
}

variable "assets_log_age" {
  type    = number
  default = 7
}

variable "assets_main_page_suffix" {
  type    = string
  default = "index.html"
}

variable "assets_not_found_page" {
  type    = string
  default = "404.html"
}

variable "assets_cors" {
  description = "Set of maps of mixed type attributes for CORS values. See appropriate attribute types here: https://www.terraform.io/docs/providers/google/r/storage_bucket.html#cors"
  type = set(object({
    origin          = optional(list(string))
    method          = optional(list(string))
    response_header = optional(list(string))
    max_age_seconds = optional(number)
  }))
  default = []
}

variable "registry_days_since_noncurrent_time" {
  type        = number
  description = "Versioned objects in registry that were soft-deleted this many days ago will be hard-deleted"
  default     = 3650 # 10 years
}

# Resource labels
variable "labels" {
  type        = map(map(string))
  description = "Labels to add to each of the managed resources."
  default     = {}
}

variable "prometheus_sa_id" {
  type        = string
  description = "Prometheus service account ID (defaults to `{environment}-prometheus-sa`)"
  default     = null
}

# Source https://github.com/terraform-google-modules/terraform-google-cloud-storage/blob/master/variables.tf
variable "registry_lifecycle_rules" {
  type = set(object({
    # Object with keys:
    # - type - The type of the action of this Lifecycle Rule. Supported values: Delete and SetStorageClass.
    # - storage_class - (Required if action type is SetStorageClass) The target Storage Class of objects affected by this Lifecycle Rule.
    action = map(string)

    # Object with keys:
    # - age - (Optional) Minimum age of an object in days to satisfy this condition.
    # - created_before - (Optional) Creation date of an object in RFC 3339 (e.g. 2017-06-13) to satisfy this condition.
    # - with_state - (Optional) Match to live and/or archived objects. Supported values include: "LIVE", "ARCHIVED", "ANY".
    # - matches_storage_class - (Optional) Comma delimited string for storage class of objects to satisfy this condition. Supported values include: MULTI_REGIONAL, REGIONAL, NEARLINE, COLDLINE, STANDARD, DURABLE_REDUCED_AVAILABILITY.
    # - matches_prefix - (Optional) One or more matching name prefixes to satisfy this condition.
    # - matches_suffix - (Optional) One or more matching name suffixes to satisfy this condition.
    # - num_newer_versions - (Optional) Relevant only for versioned objects. The number of newer versions of an object to satisfy this condition.
    # - custom_time_before - (Optional) A date in the RFC 3339 format YYYY-MM-DD. This condition is satisfied when the customTime metadata for the object is set to an earlier date than the date used in this lifecycle condition.
    # - days_since_custom_time - (Optional) The number of days from the Custom-Time metadata attribute after which this condition becomes true.
    # - days_since_noncurrent_time - (Optional) Relevant only for versioned objects. Number of days elapsed since the noncurrent timestamp of an object.
    # - noncurrent_time_before - (Optional) Relevant only for versioned objects. The date in RFC 3339 (e.g. 2017-06-13) when the object became nonconcurrent.
    condition = map(string)
  }))
  description = "List of registry lifecycle rules to configure. Format is the same as described in provider documentation https://www.terraform.io/docs/providers/google/r/storage_bucket.html#lifecycle_rule except condition.matches_storage_class should be a comma delimited string."
  default     = []
}

variable "registry_autoclass_enabled" {
  description = "Enables autoclass on the registry bucket"
  default     = false
  type        = bool
}

variable "registry_autoclass_terminal_class" {
  description = "Sets the lowest storage class that Autoclass will move objects to"
  default     = "ARCHIVE"
  type        = string
}

variable "archive_sink_filter" {
  description = "Sets the filter for the log sink when the logging-archive-sink is enabled"
  default     = "resource.type=\"gce_instance\""
  type        = string
}
