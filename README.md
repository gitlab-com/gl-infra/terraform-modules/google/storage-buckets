# GitLab.com Storage Buckets Terraform Module

## What is this?

This module provisions various GCS buckets.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.4 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_logging_project_sink.log](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/logging_project_sink) | resource |
| [google_project_iam_member.project-log-creator](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_service_account.deleted-project-group-exports](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [google_service_account.periodic-host-profile](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [google_service_account.prometheus-sa](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [google_service_account.redis-analysis](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [google_service_account.thanos-rules](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [google_storage_bucket.artifacts](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.assets](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.assets-logs](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.backups](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.backups-tmp](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.chef-bootstrap](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.ci-cache](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.ci-secure-files](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.deleted-project-group-exports](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.dependency-proxy](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.external-diffs](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.flightapi](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.gitlab-pages](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.lfs-objects](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.log](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.package-repo](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.periodic-host-profile](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.prometheus](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.redis-analysis](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.registry](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.security](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.storage-logs](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.terraform-state](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.thanos-rules](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.uploads](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket_iam_member.archived-logs-creator-creator](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.archived-logs-legacy-viewer-reader](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.archived-logs-viewer](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.artifacts-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.assets-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.backups-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.backups-tmp-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.chef-bootstrap-viewer](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.ci-cache](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.ci-secure-files-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.deleted-project-group-exports](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.dependency-proxy-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.external-diffs-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.flightapi_gitlab_kas](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.gitlab-pages-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.lfs-objects-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.member](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.package-repo-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.periodic-host-profile-admin-member-service-account](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.periodic-host-profile-viewer-member-sg](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.prometheus-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.redis-analysis-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.registry-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.registry-cdn-viewer](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.secrets-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.secrets-viewer](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.security-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.storage-analytics](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.terraform-state-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.thanos-rules-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.uploads-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_project.project](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/project) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_archive_sink_filter"></a> [archive\_sink\_filter](#input\_archive\_sink\_filter) | Sets the filter for the log sink when the logging-archive-sink is enabled | `string` | `"resource.type=\"gce_instance\""` | no |
| <a name="input_archived_logs_archive_move_age"></a> [archived\_logs\_archive\_move\_age](#input\_archived\_logs\_archive\_move\_age) | Objects older than this many days will be moved to archive storage | `number` | `15` | no |
| <a name="input_archived_logs_creator_access"></a> [archived\_logs\_creator\_access](#input\_archived\_logs\_creator\_access) | Accounts which require object creator access to archived logs | `list(string)` | `[]` | no |
| <a name="input_archived_logs_deletion_age"></a> [archived\_logs\_deletion\_age](#input\_archived\_logs\_deletion\_age) | Objects older than this many days will be deleted | `number` | `365` | no |
| <a name="input_archived_logs_storage_class"></a> [archived\_logs\_storage\_class](#input\_archived\_logs\_storage\_class) | n/a | `string` | `"MULTI_REGIONAL"` | no |
| <a name="input_archived_logs_viewer_access"></a> [archived\_logs\_viewer\_access](#input\_archived\_logs\_viewer\_access) | Accounts which require viewer access to archived logs | `list(string)` | `[]` | no |
| <a name="input_artifact_age"></a> [artifact\_age](#input\_artifact\_age) | n/a | `number` | `7` | no |
| <a name="input_artifacts_autoclass_enabled"></a> [artifacts\_autoclass\_enabled](#input\_artifacts\_autoclass\_enabled) | Enables autoclass on the artifacts bucket | `bool` | `false` | no |
| <a name="input_artifacts_autoclass_terminal_class"></a> [artifacts\_autoclass\_terminal\_class](#input\_artifacts\_autoclass\_terminal\_class) | Sets the lowest storage class that Autoclass will move objects to | `string` | `"ARCHIVE"` | no |
| <a name="input_artifacts_versioning"></a> [artifacts\_versioning](#input\_artifacts\_versioning) | n/a | `bool` | `true` | no |
| <a name="input_assets_cors"></a> [assets\_cors](#input\_assets\_cors) | Set of maps of mixed type attributes for CORS values. See appropriate attribute types here: https://www.terraform.io/docs/providers/google/r/storage_bucket.html#cors | <pre>set(object({<br>    origin          = optional(list(string))<br>    method          = optional(list(string))<br>    response_header = optional(list(string))<br>    max_age_seconds = optional(number)<br>  }))</pre> | `[]` | no |
| <a name="input_assets_log_age"></a> [assets\_log\_age](#input\_assets\_log\_age) | n/a | `number` | `7` | no |
| <a name="input_assets_logs_versioning"></a> [assets\_logs\_versioning](#input\_assets\_logs\_versioning) | n/a | `bool` | `false` | no |
| <a name="input_assets_main_page_suffix"></a> [assets\_main\_page\_suffix](#input\_assets\_main\_page\_suffix) | n/a | `string` | `"index.html"` | no |
| <a name="input_assets_not_found_page"></a> [assets\_not\_found\_page](#input\_assets\_not\_found\_page) | n/a | `string` | `"404.html"` | no |
| <a name="input_assets_versioning"></a> [assets\_versioning](#input\_assets\_versioning) | n/a | `bool` | `true` | no |
| <a name="input_chef_bootstrap_versioning"></a> [chef\_bootstrap\_versioning](#input\_chef\_bootstrap\_versioning) | n/a | `bool` | `true` | no |
| <a name="input_ci_secure_files_age"></a> [ci\_secure\_files\_age](#input\_ci\_secure\_files\_age) | n/a | `number` | `30` | no |
| <a name="input_ci_secure_files_versioning"></a> [ci\_secure\_files\_versioning](#input\_ci\_secure\_files\_versioning) | n/a | `bool` | `true` | no |
| <a name="input_dependency_proxy_age"></a> [dependency\_proxy\_age](#input\_dependency\_proxy\_age) | n/a | `number` | `30` | no |
| <a name="input_dependency_proxy_storage_class"></a> [dependency\_proxy\_storage\_class](#input\_dependency\_proxy\_storage\_class) | n/a | `string` | `"STANDARD"` | no |
| <a name="input_dependency_proxy_versioning"></a> [dependency\_proxy\_versioning](#input\_dependency\_proxy\_versioning) | n/a | `bool` | `false` | no |
| <a name="input_enable_buckets"></a> [enable\_buckets](#input\_enable\_buckets) | Toggle bucket creation. | <pre>object({<br>    artifacts                     = optional(bool, true)<br>    assets                        = optional(bool, true)<br>    assets-logs                   = optional(bool, true)<br>    backups                       = optional(bool, false)<br>    chef-bootstrap                = optional(bool, true)<br>    ci-cache                      = optional(bool, false)<br>    ci-secure-files               = optional(bool, true)<br>    deleted-project-group-exports = optional(bool, false)<br>    dependency-proxy              = optional(bool, true)<br>    external-diffs                = optional(bool, true)<br>    flightapi                     = optional(bool, false)<br>    lfs-objects                   = optional(bool, true)<br>    logging-archive               = optional(bool, true)<br>    logging-archive-sink          = optional(bool, false)<br>    package-repo                  = optional(bool, true)<br>    pages                         = optional(bool, true)<br>    periodic-host-profile         = optional(bool, true)<br>    prometheus                    = optional(bool, true)<br>    redis-analysis                = optional(bool, true)<br>    registry                      = optional(bool, true)<br>    secrets                       = optional(bool, true)<br>    security                      = optional(bool, true)<br>    storage-logs                  = optional(bool, true)<br>    terraform-state               = optional(bool, true)<br>    thanos-rules                  = optional(bool, false)<br>    uploads                       = optional(bool, true)<br>  })</pre> | `{}` | no |
| <a name="input_enable_registry_cdn"></a> [enable\_registry\_cdn](#input\_enable\_registry\_cdn) | n/a | `bool` | `false` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Storage buckets that are common across all environments | `string` | n/a | yes |
| <a name="input_external_diffs_age"></a> [external\_diffs\_age](#input\_external\_diffs\_age) | n/a | `number` | `30` | no |
| <a name="input_external_diffs_versioning"></a> [external\_diffs\_versioning](#input\_external\_diffs\_versioning) | n/a | `bool` | `true` | no |
| <a name="input_gcp_security_group_host_profiles_email"></a> [gcp\_security\_group\_host\_profiles\_email](#input\_gcp\_security\_group\_host\_profiles\_email) | A Google Group containing team members who need viewer permissions for the 'periodic-host-profile' bucket | `string` | `"gcp-host-profiles-sg@gitlab.com"` | no |
| <a name="input_gcs_service_account_emails"></a> [gcs\_service\_account\_emails](#input\_gcs\_service\_account\_emails) | Email address of the service accounts for the GitLab deployment to grant access to its GCS buckets | `set(string)` | n/a | yes |
| <a name="input_gcs_storage_analytics_group_email"></a> [gcs\_storage\_analytics\_group\_email](#input\_gcs\_storage\_analytics\_group\_email) | https://cloud.google.com/storage/docs/access-logs | `string` | `"cloud-storage-analytics@google.com"` | no |
| <a name="input_kas_service_account_emails"></a> [kas\_service\_account\_emails](#input\_kas\_service\_account\_emails) | The email of the service accounts for GitLab KAS | `set(string)` | `[]` | no |
| <a name="input_labels"></a> [labels](#input\_labels) | Labels to add to each of the managed resources. | `map(map(string))` | `{}` | no |
| <a name="input_lfs_object_age"></a> [lfs\_object\_age](#input\_lfs\_object\_age) | n/a | `number` | `30` | no |
| <a name="input_lfs_objects_versioning"></a> [lfs\_objects\_versioning](#input\_lfs\_objects\_versioning) | n/a | `bool` | `true` | no |
| <a name="input_package_repo_age"></a> [package\_repo\_age](#input\_package\_repo\_age) | n/a | `number` | `30` | no |
| <a name="input_package_repo_versioning"></a> [package\_repo\_versioning](#input\_package\_repo\_versioning) | n/a | `bool` | `true` | no |
| <a name="input_pages_objects_versioning"></a> [pages\_objects\_versioning](#input\_pages\_objects\_versioning) | GitLab Pages by their nature are versioned in git | `bool` | `false` | no |
| <a name="input_periodic_host_profile_log_age"></a> [periodic\_host\_profile\_log\_age](#input\_periodic\_host\_profile\_log\_age) | Delete profiles older than this many days | `number` | `30` | no |
| <a name="input_project"></a> [project](#input\_project) | n/a | `string` | n/a | yes |
| <a name="input_prometheus_sa_id"></a> [prometheus\_sa\_id](#input\_prometheus\_sa\_id) | Prometheus service account ID (defaults to `{environment}-prometheus-sa`) | `string` | `null` | no |
| <a name="input_prometheus_versioning"></a> [prometheus\_versioning](#input\_prometheus\_versioning) | n/a | `bool` | `true` | no |
| <a name="input_redis_analysis_log_age"></a> [redis\_analysis\_log\_age](#input\_redis\_analysis\_log\_age) | Redis analysis logs older than this many days will be deleted | `number` | `1825` | no |
| <a name="input_redis_analysis_versioning"></a> [redis\_analysis\_versioning](#input\_redis\_analysis\_versioning) | Generally no need for versioning here; files will be date-versioned | `bool` | `false` | no |
| <a name="input_registry_autoclass_enabled"></a> [registry\_autoclass\_enabled](#input\_registry\_autoclass\_enabled) | Enables autoclass on the registry bucket | `bool` | `false` | no |
| <a name="input_registry_autoclass_terminal_class"></a> [registry\_autoclass\_terminal\_class](#input\_registry\_autoclass\_terminal\_class) | Sets the lowest storage class that Autoclass will move objects to | `string` | `"ARCHIVE"` | no |
| <a name="input_registry_days_since_noncurrent_time"></a> [registry\_days\_since\_noncurrent\_time](#input\_registry\_days\_since\_noncurrent\_time) | Versioned objects in registry that were soft-deleted this many days ago will be hard-deleted | `number` | `3650` | no |
| <a name="input_registry_lifecycle_rules"></a> [registry\_lifecycle\_rules](#input\_registry\_lifecycle\_rules) | List of registry lifecycle rules to configure. Format is the same as described in provider documentation https://www.terraform.io/docs/providers/google/r/storage_bucket.html#lifecycle_rule except condition.matches\_storage\_class should be a comma delimited string. | <pre>set(object({<br>    # Object with keys:<br>    # - type - The type of the action of this Lifecycle Rule. Supported values: Delete and SetStorageClass.<br>    # - storage_class - (Required if action type is SetStorageClass) The target Storage Class of objects affected by this Lifecycle Rule.<br>    action = map(string)<br><br>    # Object with keys:<br>    # - age - (Optional) Minimum age of an object in days to satisfy this condition.<br>    # - created_before - (Optional) Creation date of an object in RFC 3339 (e.g. 2017-06-13) to satisfy this condition.<br>    # - with_state - (Optional) Match to live and/or archived objects. Supported values include: "LIVE", "ARCHIVED", "ANY".<br>    # - matches_storage_class - (Optional) Comma delimited string for storage class of objects to satisfy this condition. Supported values include: MULTI_REGIONAL, REGIONAL, NEARLINE, COLDLINE, STANDARD, DURABLE_REDUCED_AVAILABILITY.<br>    # - matches_prefix - (Optional) One or more matching name prefixes to satisfy this condition.<br>    # - matches_suffix - (Optional) One or more matching name suffixes to satisfy this condition.<br>    # - num_newer_versions - (Optional) Relevant only for versioned objects. The number of newer versions of an object to satisfy this condition.<br>    # - custom_time_before - (Optional) A date in the RFC 3339 format YYYY-MM-DD. This condition is satisfied when the customTime metadata for the object is set to an earlier date than the date used in this lifecycle condition.<br>    # - days_since_custom_time - (Optional) The number of days from the Custom-Time metadata attribute after which this condition becomes true.<br>    # - days_since_noncurrent_time - (Optional) Relevant only for versioned objects. Number of days elapsed since the noncurrent timestamp of an object.<br>    # - noncurrent_time_before - (Optional) Relevant only for versioned objects. The date in RFC 3339 (e.g. 2017-06-13) when the object became nonconcurrent.<br>    condition = map(string)<br>  }))</pre> | `[]` | no |
| <a name="input_registry_versioning"></a> [registry\_versioning](#input\_registry\_versioning) | n/a | `bool` | `true` | no |
| <a name="input_secrets_admin_access"></a> [secrets\_admin\_access](#input\_secrets\_admin\_access) | Accounts which require admin access to our environment secrets | `list(string)` | `[]` | no |
| <a name="input_secrets_viewer_access"></a> [secrets\_viewer\_access](#input\_secrets\_viewer\_access) | Accounts which require viewer access to our environment secrets | `list(string)` | `[]` | no |
| <a name="input_security_versioning"></a> [security\_versioning](#input\_security\_versioning) | n/a | `bool` | `true` | no |
| <a name="input_service_account_email"></a> [service\_account\_email](#input\_service\_account\_email) | n/a | `string` | n/a | yes |
| <a name="input_service_account_email_ops"></a> [service\_account\_email\_ops](#input\_service\_account\_email\_ops) | n/a | `string` | `"asset-uploader@gitlab-ops.iam.gserviceaccount.com"` | no |
| <a name="input_storage_class"></a> [storage\_class](#input\_storage\_class) | n/a | `string` | `"STANDARD"` | no |
| <a name="input_storage_log_age"></a> [storage\_log\_age](#input\_storage\_log\_age) | n/a | `number` | `7` | no |
| <a name="input_storage_logs_versioning"></a> [storage\_logs\_versioning](#input\_storage\_logs\_versioning) | n/a | `bool` | `false` | no |
| <a name="input_terraform_state_age"></a> [terraform\_state\_age](#input\_terraform\_state\_age) | n/a | `number` | `30` | no |
| <a name="input_terraform_state_versioning"></a> [terraform\_state\_versioning](#input\_terraform\_state\_versioning) | n/a | `bool` | `true` | no |
| <a name="input_thanos_rules_storage_class"></a> [thanos\_rules\_storage\_class](#input\_thanos\_rules\_storage\_class) | n/a | `string` | `"STANDARD"` | no |
| <a name="input_thanos_rules_versioning"></a> [thanos\_rules\_versioning](#input\_thanos\_rules\_versioning) | Generally no need for versioning here; rules are git versioned | `bool` | `false` | no |
| <a name="input_upload_age"></a> [upload\_age](#input\_upload\_age) | n/a | `number` | `7` | no |
| <a name="input_uploads_versioning"></a> [uploads\_versioning](#input\_uploads\_versioning) | n/a | `bool` | `true` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_chef_bootstrap_bucket_name"></a> [chef\_bootstrap\_bucket\_name](#output\_chef\_bootstrap\_bucket\_name) | n/a |
| <a name="output_logging_bucket_name"></a> [logging\_bucket\_name](#output\_logging\_bucket\_name) | n/a |
| <a name="output_prometheus_service_account_email"></a> [prometheus\_service\_account\_email](#output\_prometheus\_service\_account\_email) | n/a |
| <a name="output_prometheus_service_account_name"></a> [prometheus\_service\_account\_name](#output\_prometheus\_service\_account\_name) | n/a |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
