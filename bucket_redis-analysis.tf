resource "google_storage_bucket" "redis-analysis" {
  count = var.enable_buckets.redis-analysis ? 1 : 0

  name     = "gitlab-${var.environment}-redis-analysis"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = var.redis_analysis_versioning
  }

  storage_class = var.storage_class

  labels = merge(lookup(var.labels, "infrastructure", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-redis-analysis"
  })

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age        = var.redis_analysis_log_age
      with_state = "LIVE"
    }
  }
}

resource "google_storage_bucket_iam_member" "redis-analysis-admin" {
  count = var.enable_buckets.redis-analysis ? 1 : 0

  bucket = google_storage_bucket.redis-analysis[0].name
  role   = "roles/storage.objectAdmin"
  member = google_service_account.redis-analysis[0].member
}
