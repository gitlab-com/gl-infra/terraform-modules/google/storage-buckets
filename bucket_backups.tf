resource "google_storage_bucket" "backups" {
  count = var.enable_buckets.backups ? 1 : 0

  name     = "gitlab-${var.environment}-backups"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = var.artifacts_versioning
  }

  storage_class = var.storage_class

  labels = merge(lookup(var.labels, "shared", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-backups"
  })

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }
}

resource "google_storage_bucket" "backups-tmp" {
  count = var.enable_buckets.backups ? 1 : 0

  name     = "gitlab-${var.environment}-backups-tmp"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = var.artifacts_versioning
  }

  storage_class = var.storage_class

  labels = merge(lookup(var.labels, "shared", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-backups-tmp"
  })
}

resource "google_storage_bucket_iam_member" "backups-admin" {
  for_each = var.enable_buckets.backups ? var.gcs_service_account_emails : []

  bucket = google_storage_bucket.backups[0].name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${each.value}"
}

resource "google_storage_bucket_iam_member" "backups-tmp-admin" {
  for_each = var.enable_buckets.backups ? var.gcs_service_account_emails : []

  bucket = google_storage_bucket.backups-tmp[0].name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${each.value}"
}