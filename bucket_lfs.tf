resource "google_storage_bucket" "lfs-objects" {
  count = var.enable_buckets.lfs-objects ? 1 : 0

  name     = "gitlab-${var.environment}-lfs-objects"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = var.lfs_objects_versioning
  }

  storage_class = var.storage_class

  labels = merge(lookup(var.labels, "git_lfs", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-lfs-objects"
  })

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age        = var.lfs_object_age
      with_state = "ARCHIVED"
    }
  }

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }
}

resource "google_storage_bucket_iam_member" "lfs-objects-admin" {
  for_each = var.enable_buckets.lfs-objects ? var.gcs_service_account_emails : []

  bucket = google_storage_bucket.lfs-objects[0].name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${each.value}"
}
