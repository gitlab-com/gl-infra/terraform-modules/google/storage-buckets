resource "google_storage_bucket" "deleted-project-group-exports" {
  count = var.enable_buckets.deleted-project-group-exports ? 1 : 0

  name     = "gl-${var.environment}-deleted-project-group-exports"
  project  = var.project
  location = local.storage_location

  uniform_bucket_level_access = true
  public_access_prevention    = "enforced"

  lifecycle_rule {
    condition {
      age = 30
    }
    action {
      type = "Delete"
    }
  }
  labels = merge(lookup(var.labels, "import_and_integrate", {}), {
    gitlab_com_type = "deleted-project-group-exports",
    type            = "deleted-project-group-exports",
    tfmanaged       = "yes"
  })
}

resource "google_storage_bucket_iam_member" "deleted-project-group-exports" {
  count = var.enable_buckets.deleted-project-group-exports ? 1 : 0

  bucket = google_storage_bucket.deleted-project-group-exports[0].name
  role   = "roles/storage.objectViewer"
  member = google_service_account.deleted-project-group-exports[0].member
}
