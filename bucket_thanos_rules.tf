resource "google_storage_bucket" "thanos-rules" {
  count = var.enable_buckets.thanos-rules ? 1 : 0

  name     = "gitlab-${var.environment}-thanos-rules"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = var.thanos_rules_versioning
  }

  storage_class = var.thanos_rules_storage_class

  labels = merge(lookup(var.labels, "infrastructure", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-thanos-rules"
  })

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }
}

resource "google_storage_bucket_iam_member" "thanos-rules-admin" {
  count = var.enable_buckets.thanos-rules ? 1 : 0

  bucket = google_storage_bucket.thanos-rules[0].name
  role   = "roles/storage.objectAdmin"
  member = google_service_account.thanos-rules[0].member
}

# Make bucket public. It only stores the thanos-rules runbook folder
# which is already public
resource "google_storage_bucket_iam_member" "member" {
  count = var.enable_buckets.thanos-rules ? 1 : 0

  bucket = google_storage_bucket.thanos-rules[0].name
  role   = "roles/storage.objectViewer"
  member = "allUsers"
}
