resource "google_storage_bucket" "prometheus" {
  count = var.enable_buckets.prometheus ? 1 : 0

  name     = "gitlab-${var.environment}-prometheus"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = var.prometheus_versioning
  }

  storage_class = var.storage_class

  # COST NOTE: keep the conditions here in sync with the Thanos compactor retention times to avoid increased cost of retrieval fees (data is recreated)
  # https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/tree/master/releases/thanos/buckets
  lifecycle_rule {
    action {
      type          = "SetStorageClass"
      storage_class = "NEARLINE"
    }

    condition {
      # 6.5 months to allow some time for downsampling, 5m downsample is triggered at 180d
      age        = 30 * 6.5
      with_state = "ANY"
    }
  }

  lifecycle_rule {
    action {
      type          = "SetStorageClass"
      storage_class = "COLDLINE"
    }

    condition {
      # 25 months to allow some time for downsampling, 1h downsample is triggered at 2y
      age        = 30 * 25
      with_state = "ANY"
    }
  }

  # Delete older object versions
  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      days_since_noncurrent_time = 7
    }
  }

  # Delete incomplete uploads
  lifecycle_rule {
    action {
      type = "AbortIncompleteMultipartUpload"
    }

    condition {
      age = 1
    }
  }

  labels = merge(lookup(var.labels, "shared", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-prometheus"
  })

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }
}

resource "google_storage_bucket_iam_member" "prometheus-admin" {
  for_each = var.enable_buckets.prometheus ? setunion(
    formatlist("serviceAccount:%s", var.gcs_service_account_emails),
    // `google_service_account.prometheus-sa.email` can't be used here as it
    // would result in an undeterminable for_each value when the service
    // account hasn't been created yet, as it would only be known after apply
    ["serviceAccount:${local.prometheus_sa_id}@${var.project}.iam.gserviceaccount.com"]
  ) : []

  bucket = google_storage_bucket.prometheus[0].name
  role   = "roles/storage.objectAdmin"
  member = each.value

  depends_on = [
    google_service_account.prometheus-sa
  ]
}
