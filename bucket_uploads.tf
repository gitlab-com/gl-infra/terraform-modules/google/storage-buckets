resource "google_storage_bucket" "uploads" {
  count = var.enable_buckets.uploads ? 1 : 0

  name     = "gitlab-${var.environment}-uploads"
  location = local.storage_location

  uniform_bucket_level_access = true

  versioning {
    enabled = var.uploads_versioning
  }

  storage_class = var.storage_class

  labels = merge(lookup(var.labels, "shared", {}), {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-uploads"
  })

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age        = var.upload_age
      with_state = "ARCHIVED"
    }
  }

  logging {
    log_bucket = google_storage_bucket.storage-logs[0].name
  }
}

resource "google_storage_bucket_iam_member" "uploads-admin" {
  for_each = var.enable_buckets.uploads ? var.gcs_service_account_emails : []

  bucket = google_storage_bucket.uploads[0].name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${each.value}"
}
