# Prometheus
resource "google_service_account" "prometheus-sa" {
  count = var.enable_buckets.prometheus ? 1 : 0

  account_id   = local.prometheus_sa_id
  display_name = local.prometheus_sa_id
}

# Redis - used for putting redis analysis reports into GCS
resource "google_service_account" "redis-analysis" {
  count = var.enable_buckets.redis-analysis ? 1 : 0

  account_id   = "redis-analysis"
  display_name = "redis-analysis"
}

# Host profiling - used for uploading profiles into GCS
resource "google_service_account" "periodic-host-profile" {
  count = var.enable_buckets.periodic-host-profile ? 1 : 0

  account_id   = "periodic-host-profile"
  display_name = "periodic-host-profile"
}

# Thanos rules
resource "google_service_account" "thanos-rules" {
  count = var.enable_buckets.thanos-rules ? 1 : 0

  account_id   = "thanos-rules"
  display_name = "thanos-rules"
}

# Deleted projects or groups export to a bucket
resource "google_service_account" "deleted-project-group-exports" {
  count        = var.enable_buckets.deleted-project-group-exports ? 1 : 0
  account_id   = "deleted-project-group-exports"
  display_name = "deleted-project-group-exports"
  description  = "SA for access to deleted-project-group-exports bucket"
}
